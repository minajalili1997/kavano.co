export const TaskListShow = (dispatch)=>{
    dispatch({
        type: 'TASK_LIST_SHOW'
    })
}
export const TaskListHidden = (dispatch)=>{
    dispatch({
        type: 'TASK_LIST_HIDDEN'
    })
}
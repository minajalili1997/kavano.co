export const ModalVisible = (dispatch)=>{
    dispatch({
        type: 'MODAL_VISIBILITY_SHOW'
    })
}
export const ModalHidden = (dispatch)=>{
    dispatch({
        type: 'MODAL_VISIBILITY_HIDDEN'
    })
}

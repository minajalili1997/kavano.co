import React from 'react';
import './TaskBar.scss'

function UserUi(){


    return(
        <div className="UserUi">
            <div className="user--pic">
                <img src="
                https://images.unsplash.com/photo-1488371934083-edb7857977df?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=328&q=80
                " alt="" />
            </div>
            <div className="user--detail" >
                <p>
                    Good morning,
                </p>
                <h3>
                nate!
                </h3>
            </div>
        </div>
    )
}
export default UserUi;